/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katner.grafika;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.IntStream;

/**
 * @author katnerm
 */
public class BezierPanel extends JPanel {

    private ArrayList<Rectangle> rectangles = new ArrayList<>();
    private Rectangle movedRectangle = null;

    public BezierPanel() {
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    Iterator<Rectangle> it = rectangles.iterator();
                    while (it.hasNext()) {
                        Rectangle rect = it.next();
                        if (rect.contains(e.getX(), e.getY())) {
                            it.remove();
                        }
                    }
                } else {
                    rectangles.add(new Rectangle(e.getX() - 5, e.getY() - 5, 10, 10));
                }
                repaint();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                movedRectangle = null;
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        this.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (movedRectangle == null) {
                    for (Rectangle rect : rectangles) {
                        if (rect.contains(e.getX(), e.getY())) {
                            movedRectangle = rect;
                        }
                    }
                }
                if (movedRectangle != null) {
                    movedRectangle.setLocation(e.getX() - 5, e.getY() - 5);
                    repaint();
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
    }

    public static long newtonBinomial(int n, int k) {
        if (k > n - k)
            k = n - k;

        long b = 1;
        for (int i = 1, m = n; i <= k; i++, m--)
            b = b * m / i;
        return b;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHints(rh);
        for (Rectangle rect : rectangles) {
            g2.fillRect(rect.x, rect.y, rect.width, rect.height);
        }
        g2.setColor(Color.lightGray);
        for (int i = 1; i < rectangles.size(); i++) {
            Rectangle prev = rectangles.get(i - 1);
            Rectangle next = rectangles.get(i);
            g2.drawLine(prev.x + 5, prev.y + 5, next.x + 5, next.y + 5);
        }
        bezier(g);
    }

    private void bezier(Graphics g) {
        if (rectangles.size() >= 2) {
            Graphics2D g2 = (Graphics2D) g;
            RenderingHints rh = new RenderingHints(
                    RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHints(rh);
            ArrayList<Point> points = new ArrayList<>();
            for (double t = 0.0; t < 1.0; t += 0.001) {
                double tt = t;
                int n = rectangles.size() - 1;
                double x = IntStream.rangeClosed(0, n).mapToDouble(i -> newtonBinomial(n, i) * Math.pow(1 - tt, n - i) * Math.pow(tt, i) * (rectangles.get(i).x + 5)).sum();
                double y = IntStream.rangeClosed(0, n).mapToDouble(i -> newtonBinomial(n, i) * Math.pow(1 - tt, n - i) * Math.pow(tt, i) * (rectangles.get(i).y + 5)).sum();
                points.add(new Point((int) x, (int) y));
            }
            g2.setColor(Color.red);
            for (int i = 1; i < points.size(); i++) {
                g2.drawLine(points.get(i - 1).x, points.get(i - 1).y, points.get(i).x, points.get(i).y);
            }
        }
    }
}
