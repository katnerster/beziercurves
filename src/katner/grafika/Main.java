/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katner.grafika;

import javax.swing.*;

/**
 *
 * @author katnerm
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BezierPanel panel = new BezierPanel();
        JFrame frame = new JFrame("Krzywe Beziera");
        frame.setContentPane(panel);
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    
}
